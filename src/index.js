require('dotenv').config()
const express = require('express')
const app = express()
const { port } = require('./config')
const extractFrame = require('ffmpeg-extract-frame')
const ffmpegInstaller = require('@ffmpeg-installer/ffmpeg')
const ffmpeg = require('fluent-ffmpeg')
const uuidv4 = require('uuid/v4')
const cors = require('cors')
const bodyParser = require('body-parser')
const fs = require('fs')
var multer = require('multer')
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/videos/')
  },
  filename: function (req, file, cb) {
    cb(null, uuidv4() + '.webm')
  }
})
var upload = multer({ storage })

ffmpeg.setFfmpegPath(ffmpegInstaller.path)

module.exports = ffmpeg

let server

app.use(cors())

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use('/', express.static('public/videos'))

/*
 * Retrieve all videos
 */
app.get('/', (req, res) => {
  return fs.readdir('./public/videos', (err, data) => {
    if (err) throw err
    data = data.filter(d => d.split('.')[1] !== 'jpg' && d !== '.gitkeep').map(d => {
      const name = d.split('.')[0]
      return {
        id: name,
        url: `http://localhost:8090/${name}.webm`,
        urlThumbnail: `http://localhost:8090/${name}-thumbnail.jpg`
      }
    })
    res.send(data)
  })
})

/*
 * Post a selfie
 */
app.post('/', upload.single('file'), async (req, res) => {
  var { filename } = req.file
  filename = filename.split('.')[0]

  await extractFrame({
    input: `./public/videos/${filename}.webm`,
    output: `./public/videos/${filename}-thumbnail.jpg`,
    offset: 0
  })

  res.send({
    id: filename,
    url: `http://localhost:8090/${filename}.webm`,
    urlThumbnail: `http://localhost:8090/${filename}-thumbnail.jpg`
  })
})

/*
 * Retrieve single video
 */
app.get('/:name', (req, res) => {
  const { name } = req.params
  res.send({
    id: name,
    url: `http://localhost:8090/${name}.webm`,
    urlThumbnail: `http://localhost:8090/${name}-thumbnail.jpg`
  })
})

server = app.listen(port, () => {
  console.log(`Server listening on port ${port}`)
})

module.exports.app = app
